-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 28-Maio-2018 às 17:56
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `estoque`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE `categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Teclado'),
(2, 'Monitor'),
(3, 'Impressora'),
(4, 'Placa de Video'),
(5, 'Placa Mãe'),
(6, 'Memória'),
(7, 'Gabinete'),
(8, 'Processador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clients`
--

CREATE TABLE `clients` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '0',
  `cpf` int(11) UNSIGNED ZEROFILL NOT NULL,
  `email` varchar(200) DEFAULT '0',
  `phone` varchar(50) DEFAULT '0',
  `address_street` varchar(100) NOT NULL DEFAULT '0',
  `address_number` varchar(50) NOT NULL DEFAULT '0',
  `address_neighb` varchar(100) NOT NULL DEFAULT '0',
  `address_complement` varchar(100) DEFAULT '0',
  `address_city` varchar(100) NOT NULL DEFAULT '0',
  `address_state` varchar(100) NOT NULL DEFAULT '0',
  `address_country` varchar(100) NOT NULL DEFAULT '0',
  `address_zipcode` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `clients`
--

INSERT INTO `clients` (`id`, `name`, `cpf`, `email`, `phone`, `address_street`, `address_number`, `address_neighb`, `address_complement`, `address_city`, `address_state`, `address_country`, `address_zipcode`) VALUES
(3, 'Eduardo Nunes de Lima', 00873482255, 'nunes.eduardo@outlook.com', '92995375239', 'Rua Pinheiro Branco', '274', 'Monte das Oliveiras', 'Casa 1', 'Manaus', 'Amazonas', 'Brasil', '69093-805'),
(8, 'Davi Alfaia Izel', 00834543245, 'davisk8ever@gmail.com', '92993434543', 'Rua Salvador', '234', 'AdrianÃ³polis', 'Altos', 'Manaus', 'Amazonas', 'Brasil', '69093-000'),
(9, 'Rosely Ribeiro de Lima', 04294967295, 'rosely_rosty@gmail.com', '92992343453', 'Rua Teste', '23', 'SÃ£o JosÃ©', 'Apt 2', 'Manaus', 'Amazonas', 'Brasil', '69075-989');

-- --------------------------------------------------------

--
-- Estrutura da tabela `historic`
--

CREATE TABLE `historic` (
  `id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `permissions_group`
--

CREATE TABLE `permissions_group` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `permissions` varchar(200) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE `products` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_category` int(11) NOT NULL,
  `cod` int(30) DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `quantity` float NOT NULL DEFAULT '0',
  `min_quantity` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`id`, `id_category`, `cod`, `name`, `price`, `quantity`, `min_quantity`) VALUES
(1, 2, 12312313, 'Produto de Teste 10.0', 150, 9, 12),
(2, 5, 23322322, 'Produto Legal v2.0', 100, 5, 10),
(3, 3, 2147483647, 'Produto Testador', 500, 40, 20),
(4, 8, 2147483647, 'Produto Novo', 234, 23, 15),
(5, 2, 2147483647, 'Monitor LG', 543, 32, 23),
(6, 4, 7866454, 'Placa de Video GTX', 800, 10, 3),
(7, 5, 2147483647, 'Produto do Davi', 345, 30, 15),
(8, 7, 2147483647, 'Thermaltaker C1', 350, 25, 10),
(9, 8, 2147483647, 'Intel Core I7', 1200, 30, 10),
(10, 5, 2147483647, 'Produto Legal v2.0', 234, 30, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_number` int(11) NOT NULL DEFAULT '0',
  `email` varchar(200) NOT NULL DEFAULT '0',
  `user_pass` varchar(32) NOT NULL DEFAULT '0',
  `user_token` varchar(32) DEFAULT '0',
  `group` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `user_number`, `email`, `user_pass`, `user_token`, `group`, `name`) VALUES
(1, 123, 'nunes.eduardo1992@gmail.com', '202cb962ac59075b964b07152d234b70', '160a631f9a966c0ca78f1a1da8bda5ef', 0, 'Eduardo Nunes de Lima'),
(2, 321, 'davi@gmail.com', 'caf1a3dfb505ffed0d024130f58c5cfa', '624db47aaf97965f570cdfbc9417410c', 0, 'Davi Alfaia Izel');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `historic`
--
ALTER TABLE `historic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions_group`
--
ALTER TABLE `permissions_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `historic`
--
ALTER TABLE `historic`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions_group`
--
ALTER TABLE `permissions_group`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
