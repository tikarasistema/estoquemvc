<?php
class Clientes extends Model {

	public function getClients($offset, $limit, $s=''){
		$array = array();

		if(!empty($s)){
			$sql = "SELECT * FROM clients WHERE cpf = :cpf OR name LIKE :name LIMIT $offset, $limit";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":cpf", $s);
			$sql->bindValue(":name", "%".$s."%");
			$sql->execute();
		}else{
			$sql = "SELECT * FROM clients LIMIT $offset, $limit";
			$sql = $this->db->query($sql);
		}

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getTotalClientes($s='') {
		if(!empty($s)){
			$sql = "SELECT COUNT(*) as c FROM clients WHERE cpf = :cpf OR name LIKE :name";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":cpf", $s);
			$sql->bindValue(":name", "%".$s."%");
			$sql->execute();
		}else{
			$sql = "SELECT COUNT(*) as c FROM clients";
			$sql = $this->db->query($sql);
		}
		$sql = $sql->fetch();

		return $sql['c'];
	}

	private function verifyClient($cpf = '', $email = ''){

		$sql = $this->db->prepare("SELECT id FROM clients WHERE cpf = :cpf OR email = :email");
		$sql->bindValue(":cpf", $cpf);
		$sql->bindValue(":email", $email);
		$sql->execute();
		if($sql->rowCount() == 0){
			return true;
		} else {
			return false;
		}

	}

	public function addCliente($name, $cpf = '', $email = '', $phone = '', $address_street = '', $address_number = '', $address_neighb = '', $address_complement = '', $address_city = '', $address_state = '', $address_country = '', $address_zipcode = ''){
	
		//if($this->verifyClient($cpf, $email)){

			$sql = "INSERT INTO clients (name, cpf, email, phone, address_street, address_number, address_neighb, address_complement, address_city, address_state, address_country, address_zipcode) VALUES (:name, :cpf, :email, :phone, :address_street, :address_number, :address_neighb, :address_complement, :address_city, :address_state, :address_country, :address_zipcode)";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":name", $name);
			$sql->bindValue(":cpf", $cpf);
			$sql->bindValue(":email", $email);
			$sql->bindValue(":phone", $phone);
			$sql->bindValue(":address_street", $address_street);
			$sql->bindValue(":address_number", $address_number);
			$sql->bindValue(":address_neighb", $address_neighb);
			$sql->bindValue(":address_complement", $address_complement);
			$sql->bindValue(":address_city", $address_city);
			$sql->bindValue(":address_state", $address_state);
			$sql->bindValue(":address_country", $address_country);
			$sql->bindValue(":address_zipcode", $address_zipcode);
			$sql->execute();

			return $this->db->lastInsertId();

		//}

	}

		public function editCliente($id, $name, $cpf, $email, $phone, $address_street, $address_number, $address_neighb, $address_complement, $address_city, $address_state, $address_country, $address_zipcode){
	
		//if($this->verifyClient($cpf, $email)){

			$sql = "UPDATE clients SET name = :name, cpf = :cpf, email = :email, phone = :phone, address_street = :address_street, address_number = :address_number, address_neighb = :address_neighb, address_complement = :address_complement, address_city = :address_city, address_state = :address_state, address_country = :address_country, address_zipcode = :address_zipcode WHERE id = :id";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":name", $name);
			$sql->bindValue(":cpf", $cpf);
			$sql->bindValue(":email", $email);
			$sql->bindValue(":phone", $phone);
			$sql->bindValue(":address_street", $address_street);
			$sql->bindValue(":address_number", $address_number);
			$sql->bindValue(":address_neighb", $address_neighb);
			$sql->bindValue(":address_complement", $address_complement);
			$sql->bindValue(":address_city", $address_city);
			$sql->bindValue(":address_state", $address_state);
			$sql->bindValue(":address_country", $address_country);
			$sql->bindValue(":address_zipcode", $address_zipcode);
			$sql->bindValue(":id", $id);
			$sql->execute();

		//} else {
			//return false;
		//}

	}

	public function getClient($id) {
		$array = array();

		$sql = "SELECT * FROM clients WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
		}

		return $array;
	}

	public function deleteClient($id) {

		$sql = "DELETE FROM clients WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

	}

	public function searchClientByName($name) {
		$array = array();

		$sql = $this->db->prepare("SELECT name, id FROM clients WHERE name LIKE :name LIMIT 10");
		$sql->bindValue(':name', '%'.$name.'%');
		$sql->execute();

		if($sql->rowCount() > 0) {
			$array = $sql->fetchAll();
		}

		return $array;
	}

}