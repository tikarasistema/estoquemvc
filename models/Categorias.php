<?php
class Categorias extends Model {

	public function getCategorias(){
		$array = array();

		$sql = $this->db->query("SELECT * FROM categories");
		if($sql->rowCount() > 0 ){
			$array = $sql->fetchAll();
		}

		return $array;
	}

}