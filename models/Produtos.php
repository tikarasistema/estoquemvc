<?php
class Produtos extends Model {

	public function getProducts($offset, $limit, $s=''){
		$array = array();

		if(!empty($s)){
			$sql = "SELECT * FROM products WHERE cod = :cod OR name LIKE :name LIMIT $offset, $limit";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":cod", $s);
			$sql->bindValue(":name", "%".$s."%");
			$sql->execute();
		}else{
			$sql = "SELECT * FROM products LIMIT $offset, $limit";
			$sql = $this->db->query($sql);
		}

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getTotalProdutos($s='') {
		if(!empty($s)){
			$sql = "SELECT COUNT(*) as c FROM products WHERE cod = :cod OR name LIKE :name";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":cod", $s);
			$sql->bindValue(":name", "%".$s."%");
			$sql->execute();
		}else{
			$sql = "SELECT COUNT(*) as c FROM products";
			$sql = $this->db->query($sql);
		}
		$sql = $sql->fetch();

		return $sql['c'];
	}

	public function setHistoric($id_product, $id_user, $action) {
		$sql = $this->db->prepare("INSERT INTO historic SET id_product = :id_product, id_user = :id_user, action = :action, date_action = NOW()");
		$sql->bindValue(":id_product", $id_product);
		$sql->bindValue(":id_user", $id_user);
		$sql->bindValue(":action", $action);
		$sql->execute();
	}

	private function verifyProduct($cod, $name){
		
		$sql = $this->db->prepare("SELECT id FROM products WHERE cod = :cod OR name = :name");
		$sql->bindValue(":cod", $cod);
		$sql->bindValue(":name", $name);
		$sql->execute();
		if($sql->rowCount() == 0){
			return true;
		} else {
			return false;
		}

	}

	public function addProduct($cod, $name, $category, $price, $quant, $min_quantity){

		if($this->verifyProduct($cod, $name)){
			$sql = "INSERT INTO products (cod, name, id_category, price, quant, min_quantity) VALUES (:cod, :name, :id_category, :price, :quant, :min_quantity)";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":cod", $cod);
			$sql->bindValue(":name", $name);
			$sql->bindValue(":id_category", $category);
			$sql->bindValue(":price", $price);
			$sql->bindValue(":quant", $quant);
			$sql->bindValue(":min_quantity", $min_quantity);
			$sql->execute();

			return true;

		} else {
			return false;
		}

	}

	public function getProduct($id) {
		$array = array();

		$sql = "SELECT *, (select categories.name from categories where categories.id = products.id_category) as category FROM products WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
		}

		return $array;
	}

	public function editProduct($id, $cod, $name, $category, $price, $quant, $min_quantity){

		$sql = "UPDATE products SET cod = :cod, name = :name, id_category = :id_category, price = :price, quant = :quant, min_quantity = :min_quantity WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":cod", $cod);
		$sql->bindValue(":name", $name);
		$sql->bindValue(":id_category", $category);
		$sql->bindValue(":price", $price);
		$sql->bindValue(":quant", $quant);
		$sql->bindValue(":min_quantity", $min_quantity);
		$sql->bindValue(":id", $id);
		$sql->execute();

	}

	public function getLowQuantityProducts(){
		$array = array();

		$sql = "SELECT * FROM products WHERE quant < min_quantity";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function searchProductsByName($name) {
		$array = array();

		$sql = $this->db->prepare("SELECT name, price, id FROM products WHERE name LIKE :name LIMIT 10");
		$sql->bindValue(':name', '%'.$name.'%');
		$sql->execute();

		if($sql->rowCount() > 0) {
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function deleteProduct($id) {

		$sql = "DELETE FROM products WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

	}

	public function downStock($id_prod, $quant_prod, $id_user){

		$sql = $this->db->prepare("UPDATE products SET quant = quant - $quant_prod WHERE id = :id");
		$sql->bindValue(":id", $id_prod);
		$sql->execute();

		$this->setHistoric($id_prod, $id_user, 'Baixa');

	}

}














