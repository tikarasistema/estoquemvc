<?php
class Vendas extends Model {

	public function getList($offset){
		$array = array();

		$sql = $this->db->prepare("
			SELECT
				sales.id,
				sales.date_sale,
				sales.total_price,
				sales.status,
				clients.name
			FROM sales
			LEFT JOIN clients ON clients.id = sales.id_client
			ORDER BY sales.date_sale DESC
			LIMIT $offset, 10");
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getInfo($id) {
		$array = array();

		$sql = $this->db->prepare("
			SELECT
				*,
				( select clients.name from clients where clients.id = sales.id_client ) as client_name
			FROM sales
			WHERE 
				id = :id");
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0) {
			$array['info'] = $sql->fetch();
		}

		$sql = $this->db->prepare("
			SELECT
				sales_products.quant,
				sales_products.sale_price,
				products.name
			FROM sales_products
			LEFT JOIN products
				ON products.id = sales_products.id_product
			WHERE
				sales_products.id_sale = :id_sale");
		$sql->bindValue(":id_sale", $id);
		$sql->execute();

		if($sql->rowCount() > 0) {
			$array['products'] = $sql->fetchAll();
		}

		return $array;
	}

	public function changeStatus($status, $id) {

		$sql = $this->db->prepare("UPDATE sales SET status = :status WHERE id = :id");
		$sql->bindValue(":status", $status);
		$sql->bindValue(":id", $id);
		$sql->execute();

	}

	public function addSale($id_client, $id_user, $quant, $status){
		$p = new Produtos();

		$sql = $this->db->prepare("INSERT INTO sales SET id_client = :id_client, id_user = :id_user, date_sale = NOW(), total_price = :total_price, status = :status");
		$sql->bindValue(":id_client", $id_client);
		$sql->bindValue(":id_user", $id_user);
		$sql->bindValue(":total_price", '0');
		$sql->bindValue(":status", $status);
		$sql->execute();

		$id_sale = $this->db->lastInsertId();

		$total_price = 0;

		foreach ($quant as $id_prod => $quant_prod) {
			$sql = $this->db->prepare("SELECT price FROM products WHERE id = :id");
			$sql->bindValue(":id", $id_prod);
			$sql->execute();

			if($sql->rowCount() > 0){
				$row = $sql->fetch();
				$price = $row['price'];

				$sqlp = $this->db->prepare("INSERT INTO sales_products SET id_sale = :id_sale, id_product = :id_product, quant = :quant, sale_price = :sale_price");
				$sqlp->bindValue(":id_sale", $id_sale);
				$sqlp->bindValue(":id_product", $id_prod);
				$sqlp->bindValue(":quant", $quant_prod);
				$sqlp->bindValue(":sale_price", $price);
				$sqlp->execute();

				$p->downStock($id_prod, $quant_prod, $id_user);

				$total_price += $price * $quant_prod;
			}
		}

		$sql = $this->db->prepare("UPDATE sales SET total_price = :total_price WHERE id = :id");
		$sql->bindValue(":total_price", $total_price);
		$sql->bindValue(":id", $id_sale);
		$sql->execute();

	}	

}






