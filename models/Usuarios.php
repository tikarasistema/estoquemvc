<?php
class Usuarios extends Model {

	private $user;
	private $permissions;

	public function verifyUser($number, $pass){

		$sql = "SELECT * FROM users WHERE user_number = :unumber AND user_pass = :upass";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":unumber", $number);
		$sql->bindValue(":upass", md5($pass));
		$sql->execute();

		if($sql->rowCount() > 0){
			return true;
		} else {
			return false;
		}

	}

	public function createToken($unumber){
		$token = md5(time().rand(0,9999).time().rand(0,9999));

		$sql = "UPDATE users SET user_token = :token WHERE user_number = :unumber";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":token", $token);
		$sql->bindValue(":unumber", $unumber);
		$sql->execute();

		return $token;
	}

	public function checkLogin(){
		if(isset($_SESSION['token']) && !empty($_SESSION['token'])){
			$token = $_SESSION['token'];

			$sql = "SELECT * FROM users WHERE user_token = :token";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":token", $token);
			$sql->execute();

			if($sql->rowCount() > 0){
				$sql = $sql->fetch();
				return true;
			}	
		}
		
		return false;
	}

	public function userInfo(){
		if(!empty($_SESSION['token'])){
			$id = $_SESSION['token'];

			$sql = $this->db->prepare("SELECT * FROM users WHERE user_token = :id");
			$sql->bindValue(":id", $id);
			$sql->execute();

			if($sql->rowCount() > 0){
				$this->user = $sql->fetch();
				$this->permissions = new Permissoes();
				$this->permissions->setGroup($this->user['group']);
			}
		}

	}

	public function getName(){
		if(isset($this->user['name'])){
			return $this->user['name'];
		} else {
			return '';
		}
	}

	public function getId(){
		if(isset($this->user['id'])){
			return $this->user['id'];
		} else {
			return '';
		}
	}

	public function hasPermission($name){
		return $this->permissions->hasPermission($name);
	}

}














