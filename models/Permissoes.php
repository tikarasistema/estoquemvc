<?php
class Permissoes extends Model {

	private $group;
	private $permissions;

	public function setGroup($id){
		$this->group = $id;
		$this->permissions = array();

		$sql = $this->db->prepare("SELECT permissions FROM permissions_group WHERE id = :id");
		$sql->bindValue(':id', $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$row = $sql->fetch();

			if(empty($row['permissions'])){
				$row['permissions'] = '0';
			}

			$permissions = $row['permissions'];

			$sql = $this->db->prepare("SELECT name FROM permissions WHERE id IN($permissions)");
			$sql->execute();

			if($sql->rowCount() > 0){
				foreach($sql->fetchAll() as $item){
					$this->permissions[] = $item['name'];
				}
			}
		}

	}

	public function hasPermission($name){
		if (in_array($name, $this->permissions)) {
			return true;
		} else {
			return false;
		}
	}
	
}