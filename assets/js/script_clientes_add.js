$('#address_zipcode').on('blur', function(){
	var cep = $(this).val();

	$.ajax({
		url:'http://api.postmon.com.br/v1/cep/'+cep,
		type:'GET',
		daType:'json',
		success:function(json){
			if(typeof json.logradouro != 'undefined'){
				$('#address_street').val(json.logradouro);
				$('#address_neighb').val(json.bairro);
				$('#address_city').val(json.cidade);
				$('#address_state').val(json.estado);
				$('#address_country').val("Brasil");
			}
		}
	});
});