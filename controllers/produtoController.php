<?php
class produtoController extends Controller {

	private $user;

	public function __construct() {
		parent::__construct();

		$this->users = new Usuarios();
		if(!$this->users->checkLogin()){
			header("Location: ".BASE_URL."login");
			exit;
		}
	}

    public function index() {
        
        $data = array();
        $p = new Produtos();
        $u = new Usuarios();
        $u->userInfo();
        $data['name'] = $u->getName();

        $s = '';
        $limit = 10;

        if(!empty($_GET['busca'])){
            $s = $_GET['busca'];
        }
        $total_produtos = $p->getTotalProdutos($s);

        $data['paginas'] = ceil($total_produtos/$limit);

        $data['paginaAtual'] = 1;
        if(!empty($_GET['p'])) {
            $data['paginaAtual'] = intval($_GET['p']);
        }

        $offset = ($data['paginaAtual'] * $limit) - $limit;

        $data['list'] = $p->getProducts($offset, $limit, $s);

        $this->loadTemplate('produto', $data);
    }

	public function add() {
        $data = array();
        $p = new Produtos();
        $u = new Usuarios();
        $c = new Categorias();
        $u->userInfo();
        $data['name'] = $u->getName();
        $data['cats'] = $c->getCategorias();

        if(!empty($_POST['cod'])){
            $cod = $_POST['cod'];
            $name = $_POST['name'];
            $category = $_POST['category'];
            $price = $_POST['price'];
            $quantity = $_POST['quant'];
            $min_quantity = $_POST['min_quantity'];

            if($p->addProduct($cod, $name, $category, $price, $quant, $min_quantity)){
                $data['msg'] = '<div class="alert alert-success" role="alert">
                    Produto cadastrado com sucesso!
                </div>';    
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                    Produto já existente em estoque!
                </div>'; 
            }
            
        }

        $this->loadTemplate('addProduto', $data);
    }

    public function edit($id){
        $data = array();
        $p = new Produtos();
        $u = new Usuarios();
        $c = new Categorias();
        $u->userInfo();
        $data['name'] = $u->getName();
        $data['cats'] = $c->getCategorias();

        if(!empty($_POST['cod'])){
            $cod = $_POST['cod'];
            $name = $_POST['name'];
            $category = $_POST['category'];
            $price = $_POST['price'];
            $quantity = $_POST['quant'];
            $min_quantity = $_POST['min_quantity'];

            $p->editProduct($id, $cod, $name, $category, $price, $quant, $min_quantity);
                $data['msg'] = '<div class="alert alert-success" role="alert">
                    Produto alterado com sucesso!
                </div>';    
        }

        $data['info'] = $p->getProduct($id);

        $this->loadTemplate('editProduto', $data);
    }

    public function delete($id){
         $data = array();
         $p = new Produtos();

        if(isset($id) && !empty($id)){
            $p->deleteProduct($id);
            header("Location: ".BASE_URL."produto");
            exit;
        }

        header("Location: ".BASE_URL."produto");
        exit;

    }

}