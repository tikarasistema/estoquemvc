<?php
class ajaxController extends Controller {

	private $user;

	public function __construct() {
		parent::__construct();

		$this->users = new Usuarios();
		if(!$this->users->checkLogin()){
			header("Location: ".BASE_URL."login");
			exit;
		}
	}

	public function index(){}

	public function search_clients() {
    	$data = array();
    	$u = new Usuarios();
       	$u->userInfo();
    	$c = new Clientes();

    	if(isset($_GET['q']) && !empty($_GET['q'])) {
    		$q = addslashes($_GET['q']);

    		$clients = $c->searchClientByName($q);

    		foreach($clients as $citem) {
    			$data[] = array(
    				'name' => $citem['name'],
    				'link' => BASE_URL.'/clients/edit/'.$citem['id'],
                    'id'   => $citem['id']
    			);
    		}
    	}

    	echo json_encode($data);
    }

    public function search_products() {
        $data = array();
        $u = new Usuarios();
        $u->userInfo();
        $p = new Produtos();

        if(isset($_GET['q']) && !empty($_GET['q'])) {
            $q = addslashes($_GET['q']);
            $data = $p->searchProductsByName($q);
        }

        echo json_encode($data);
    }

    public function add_client() {
        $data = array();
        $u = new Usuarios();
        $u->userInfo();
        $c = new Clientes();

        if(isset($_POST['name']) && !empty($_POST['name'])) {
            $name = addslashes($_POST['name']);

            $data['id'] = $c->addCliente($name);
        }

        echo json_encode($data);
    }

}







