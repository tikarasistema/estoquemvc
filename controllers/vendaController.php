<?php
class vendaController extends Controller {

	private $user;

	public function __construct() {
		parent::__construct();

		$this->users = new Usuarios();
		if(!$this->users->checkLogin()){
			header("Location: ".BASE_URL."login");
			exit;
		}
	}

    public function index() {
        
        $data = array();
        $v = new Vendas();
        $u = new Usuarios();
        $u->userInfo();
        $data['name'] = $u->getName();
        $data['statuses'] = array(
            '0'=>'Aguardando Pgto.',
            '1'=>'Pago',
            '2'=>'Cancelado'
        );
        $offset = 0;

        $data['sales_list'] = $v->getList($offset);

        $this->loadTemplate('venda', $data);
    }

    public function add(){

        $data = array();
        $v = new Vendas();
        $u = new Usuarios();
        $u->userInfo();
        $data['name'] = $u->getName();

        if(isset($_POST['client_id']) && !empty($_POST['client_id'])){
            $client_id = $_POST['client_id'];
            $status = $_POST['status'];
            $quant = $_POST['quant'];



            $v->addSale($client_id, $u->getId(), $quant, $status);
            header("Location: ".BASE_URL."/venda");

        }

        $this->loadTemplate('addVenda', $data);

    }

    public function edit($id){

        $data = array();
        $v = new Vendas();
        $u = new Usuarios();
        $u->userInfo();
        $data['name'] = $u->getName();

        $data['statuses'] = array(
            '0'=>'Aguardando Pgto.',
            '1'=>'Pago',
            '2'=>'Cancelado'
        );


        if(isset($_POST['status'])) {
            $status = addslashes($_POST['status']);

            $v->changeStatus($status, $id);

            header("Location: ".BASE_URL."/venda");
        }

        $data['sales_info'] = $v->getInfo($id);

        $this->loadTemplate('editVenda', $data);

    }

}