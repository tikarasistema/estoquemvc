<?php
class clienteController extends Controller {

	private $user;

	public function __construct() {
		parent::__construct();

		$this->users = new Usuarios();
		if(!$this->users->checkLogin()){
			header("Location: ".BASE_URL."login");
			exit;
		}
	}

    public function index() {
        
        $data = array();
        $c = new Clientes();
        $u = new Usuarios();
        $u->userInfo();
        $data['name'] = $u->getName();

        $s = '';
        $limit = 10;

        if(!empty($_GET['busca'])){
            $s = $_GET['busca'];
        }

        $total_clientes = $c->getTotalClientes($s);

        $data['paginas'] = ceil($total_clientes/$limit);

        $data['paginaAtual'] = 1;
        if(!empty($_GET['p'])) {
            $data['paginaAtual'] = intval($_GET['p']);
        }

        $offset = ($data['paginaAtual'] * $limit) - $limit;

        $data['list'] = $c->getclients($offset, $limit, $s);

        $this->loadTemplate('cliente', $data);

    }

	public function add() {
        $data = array();
        $c = new Clientes();
        $u = new Usuarios();
        $u->userInfo();
        $data['name'] = $u->getName();

        if(!empty($_POST['name'])){
            $name = $_POST['name'];
            $cpf = $_POST['cpf'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $address_street = $_POST['address_street'];
            $address_number = $_POST['address_number'];
            $address_neighb = $_POST['address_neighb'];
            $address_complement = $_POST['address_complement'];
            $address_city = $_POST['address_city'];
            $address_state = $_POST['address_state'];
            $address_country = $_POST['address_country'];
            $address_zipcode = $_POST['address_zipcode'];

            if($c->addCliente($name, $cpf, $email, $phone, $address_street, $address_number, $address_neighb, $address_complement, $address_city, $address_state, $address_country, $address_zipcode)){

                $data['msg'] = '<div class="alert alert-success" role="alert">
                    Cliente cadastrado com sucesso!
                </div>';

            } else {
                        
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                    Email e/ou CPF já existentes!
                </div>';
            }

        }

        $this->loadTemplate('addCliente', $data);
    }

    public function edit($id){
        $data = array();
        $c = new Clientes();
        $u = new Usuarios();
        $u->userInfo();
        $data['name'] = $u->getName();

        if(!empty($_POST['name'])){
            $name = $_POST['name'];
            $cpf = $_POST['cpf'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $address_street = $_POST['address_street'];
            $address_number = $_POST['address_number'];
            $address_neighb = $_POST['address_neighb'];
            $address_complement = $_POST['address_complement'];
            $address_city = $_POST['address_city'];
            $address_state = $_POST['address_state'];
            $address_country = $_POST['address_country'];
            $address_zipcode = $_POST['address_zipcode'];

            $c->editCliente($id, $name, $cpf, $email, $phone, $address_street, $address_number, $address_neighb, $address_complement, $address_city, $address_state, $address_country, $address_zipcode);

                $data['msg'] = '<div class="alert alert-success" role="alert">
                    Cliente alterado com sucesso!
                </div>';            
        }

        $data['info'] = $c->getClient($id);

        $this->loadTemplate('editCliente', $data);
    }

    public function delete($id){
         $data = array();
         $c = new Clientes();

        if(isset($id) && !empty($id)){
            $c->deleteClient($id);
            header("Location: ".BASE_URL."cliente");
            exit;
        }

        header("Location: ".BASE_URL."cliente");
        exit;

    }

    public function searchClientByName($name) {
        $array = array();

        $sql = $this->db->prepare("SELECT name, id FROM clients WHERE name LIKE :name LIMIT 10");
        $sql->bindValue(':name', '%'.$name.'%');
        $sql->execute();

        if($sql->rowCount() > 0) {
            $array = $sql->fetchAll();
        }

        return $array;
    }

}