<?php
class relatorioController extends Controller {

	public function __construct() {
		parent::__construct();

		$this->users = new Usuarios();
		if(!$this->users->checkLogin()){
			header("Location: ".BASE_URL."login");
			exit;
		}
	}

    public function index() {
        $data = array();
        $p = new Produtos();

        $data['list'] = $p->getLowQuantityProducts();

        $this->loadTemplate('relatorio', $data);
    }

}