<?php
class homeController extends Controller {

	public function __construct() {
		parent::__construct();

		$u = new Usuarios();
		if(!$u->checkLogin()){
			header("Location: ".BASE_URL."login");
			exit;
		}
	}

    public function index() {
        $data = array();
        $u = new Usuarios();
        $u->userInfo();
        $data['name'] = $u->getName();

        $this->loadTemplate('home', $data);
    }

}