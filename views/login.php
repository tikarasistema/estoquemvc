<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sistema de Estoque - Login</title>
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/login.css">
</head>
<body>
	<div class="loginarea">
		<div class="titulo">Sistema de Estoque</div>
		<hr>
		<?php if(!empty($msg)): ?>
			<div class="alert alert-danger" role="alert"><?php echo $msg; ?></div>
		<?php endif; ?>
		<form method="POST">
			Usuario: <input type="text" name="number" id="number" autocomplete="off" required>
			Senha: <input type="password" name="password" id="pass" required><br><br>
			<input type="submit" value="Entrar">
		</form>
		<br />
	</div>
</body>
</html>