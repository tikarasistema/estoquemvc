<h1>Editar Cliente</h1><br>

<?php if(!empty($msg)) {echo $msg;} ?>

<form method="POST">
  <div class="form-row">
  	<div class="form-group col-md-8">
      Nome:
      <input type="text" class="form-control" name="name" id="name" value="<?php echo $info['name']; ?>" autocomplete="off" required>
    </div>
    <div class="form-group col-md-4">
      CPF:
      <input type="number" class="form-control" name="cpf" id="cpf" value="<?php echo $info['cpf']; ?>" autocomplete="off" require>
    </div>
    <div class="form-group col-md-8">
      E-mail:
      <input type="email" class="form-control" name="email" id="email" value="<?php echo $info['email']; ?>" autocomplete="off">
    </div>
    <div class="form-group col-md-4">
      Telefone:
      <input type="text" class="form-control" name="phone" id="phone" value="<?php echo $info['phone']; ?>" autocomplete="off" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-2">
      CEP:
      <input type="text" class="form-control" name="address_zipcode" id="address_zipcode" value="<?php echo $info['address_zipcode']; ?>"  autocomplete="off" required>
    </div>
  	<div class="form-group col-md-5">
      Nome da Rua:
      <input type="text" class="form-control" name="address_street" id="address_street" value="<?php echo $info['address_street']; ?>" autocomplete="off" required>
    </div>
    <div class="form-group col-md-1">
      Nº
      <input type="text" class="form-control" name="address_number" id="address_number" value="<?php echo $info['address_number']; ?>" autocomplete="off" required>
    </div>
    <div class="form-group col-md-4">
      Bairro:
      <input type="text" class="form-control" name="address_neighb" id="address_neighb" value="<?php echo $info['address_neighb']; ?>" autocomplete="off" required>
    </div>
    <div class="form-group col-md-3">
      Complemento:
      <input type="text" class="form-control" name="address_complement" id="address_complement" value="<?php echo $info['address_complement']; ?>" autocomplete="off" required>
    </div>
  </div>
  <div class="form-row">
  	<div class="form-group col-md-4">
      Cidade:
      <input type="text" class="form-control" name="address_city" id="address_city" value="<?php echo $info['address_city']; ?>" autocomplete="off" required>
    </div>
    <div class="form-group col-md-4">
      Estado:
      <input type="text" class="form-control" name="address_state" id="address_state" value="<?php echo $info['address_state']; ?>" autocomplete="off" required>
    </div>
    <div class="form-group col-md-4">
      País:
      <input type="text" class="form-control" name="address_country" id="address_country" value="<?php echo $info['address_country']; ?>" autocomplete="off" required>
    </div>
  </div>

	<input type="submit" value="Salvar Alterações" class="btn btn-secondary btnform" /><a class="btn btn-secondary add btnform" href="<?php echo BASE_URL; ?>cliente" role="button" >Voltar</a><br /><br /><br /><br />

</form>