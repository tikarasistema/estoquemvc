<form method="GET">
	<input type="text" id="busca" value="<?php echo (!empty($_GET['busca']))?$_GET['busca']:''; ?>" name="busca" placeholder="Digite o código de barras ou o nome do produto" style="font-size: 18px;"/>
</form>
<br/>
<a class="btn btn-secondary add" href="<?php echo BASE_URL; ?>produto/add" role="button">Adicionar Produto</a><br><br>

<div class="table-responsive">
	<table class="table table-hover table-sm">
		<thead>
		<tr>
			<th>Cód.</th>
			<th>Nome do Produto</th>
			<th>Preço</th>
			<th>Quantidade</th>
			<th>Status</th>
			<th>Ações</th>
		</tr>
		</thead>
		<?php foreach($list as $item): ?>
			<tbody>
			<tr>
				<td><?php echo $item['cod']; ?></td>
				<td><?php echo $item['name']; ?></td>
				<td>R$ <?php echo number_format($item['price'], 2, ',', '.'); ?></td>
				<td><?php echo $item['quant']; ?></td>
				<td>
					<?php
						if($item['quant'] <= $item['min_quantity']){
							echo "<div class='estoque_baixo'>Estoque Baixo</div>";
						} else {
							echo "<div class='estoque_bom'>Estoque Bom</div>";
						}
					?>
				</td>
				<td>
					<a href="<?php echo BASE_URL; ?>produto/edit/<?php echo $item['id']; ?>" role="button"><img src="<?php echo BASE_URL; ?>assets/images/edit.png" width="30" title="Editar"></a> | <a href="<?php echo BASE_URL; ?>produto/delete/<?php echo $item['id']; ?>" data-confirm="Tem certeza que deseja apagar o item selecionado?" role="button"><img src="<?php echo BASE_URL; ?>assets/images/delete.png" width="30" title="Excluir"></a>
				</td>
			</tr>
			</tbody>
		<?php endforeach; ?>
	</table>
</div>
<hr>
<div class="pag">
	<?php for($q=1;$q<=$paginas;$q++): ?>
		<?php if($paginaAtual == $q): ?>
			<a style="background-color: #333;" href="<?php echo BASE_URL; ?>produto?<?php
					$w = $_GET;
					$w['p'] = $q;
					echo http_build_query($w);
					?>"><?php echo ($q); ?></a>
		<?php else: ?>
			<a href="<?php echo BASE_URL; ?>produto?<?php
					$w = $_GET;
					$w['p'] = $q;
					echo http_build_query($w);
					?>"><?php echo ($q); ?></a>
		<?php endif; ?>
	<?php endfor; ?>
</div>
<hr>
<script type="text/javascript">
	document.getElementById("busca").focus();
</script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script_products_del.js"></script>