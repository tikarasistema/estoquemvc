<a class="btn btn-secondary add" href="<?php echo BASE_URL; ?>venda/add" role="button">Adicionar Venda</a><br><br>
<div class="table-responsive">
	<table class="table table-hover table-sm">
		<thead>
		<tr>
			<th>Nome do Cliente</th>
			<th>Data</th>
			<th>Status</th>
			<th>Valor</th>
			<th>Ações</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($sales_list as $sale_item): ?>
		<tr>
			<td><?php echo $sale_item['name']; ?></td>
			<td><?php echo date('d/m/Y', strtotime($sale_item['date_sale'])); ?></td>
			<td><?php echo $statuses[$sale_item['status']]; ?></td>
			<td>R$ <?php echo number_format($sale_item['total_price'], 2, ',', '.'); ?></td>
			<td><a href="<?php echo BASE_URL; ?>venda/edit/<?php echo $sale_item['id']; ?>" role="button"><img src="<?php echo BASE_URL; ?>assets/images/edit.png" width="30" title="Editar"></a></td>
		</tr>
		</tbody>
		<?php endforeach; ?>
	</table>
</div>

<hr>

<hr>