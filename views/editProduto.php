<h1>Editar Produto</h1><br>

<?php if(!empty($msg)) {echo $msg;} ?>

<form method="POST">
	
	Código de Barras:<br/>
	<input type="text" name="cod" id="cod" value="<?php echo $info['cod']; ?>" required class="form-control" autocomplete="off" /><br/>

	Nome do Produto: <br/>
	<input type="text" name="name" id="name" value="<?php echo $info['name']; ?>" required class="form-control" autocomplete="off" /><br/>

	Categoria:
	<select name="category" id="category" class="form-control">
		<option></option>
		<?php foreach ($cats as $cat): ?>
		<option value="<?php echo $cat['id']; ?>" <?php echo ($info['id_category']==$cat['id'])?'selected="selected"':''; ?>><?php echo utf8_encode($cat['name']); ?></option>
		<?php endforeach; ?>
	</select><br/ >

	Preço do Produto:<br/>
	<input type="text" name="price" id="price" value="<?php echo $info['price']; ?>" required class="form-control" autocomplete="off" /><br/>

	Quantidade:<br/>
	<input type="text" name="quantity" id="quantity" value="<?php echo $info['quantity']; ?>" required class="form-control" autocomplete="off" /><br/>

	Qtd. Mínima:<br/>
	<input type="text" name="min_quantity" id="min_quantity" value="<?php echo $info['min_quantity']; ?>" required class="form-control" autocomplete="off" /><br/>

	<input type="submit" value="Salvar Alterações" class="btn btn-secondary btnform" /><a class="btn btn-secondary add btnform" href="<?php echo BASE_URL; ?>produto" role="button" >Voltar</a><br /><br /><br /><br />

</form>