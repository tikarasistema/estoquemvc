<h1>Adicionar Venda</h1><br>

<form method="POST">
      Nome do Cliente<br>
      <input type="hidden" name="client_id">
      <input type="text" name="client_name" id="client_name" data-type="search_clients"> <button class="btn_add_client btn btn-secondary">+</button>
      <br><br>

      Status da Venda<br>
      <select name="status" id="status">
      	<option value="0">Aguardando Pgto.</option>
      	<option value="1">Pago</option>
      	<option value="2">Cancelado</option>
      </select><br><br>

      Preço da Venda<br>
      <input type="text" name="total_price" id="total_price" disabled="disabled"><br><br>

      <hr>

      <h6>Produtos</h6>

      <fieldset>
            <legend>Adicionar Produto</legend>
            <input type="text" id="add_prod" data-type="search_products"><br>
      </fieldset>
      <br>
      <table class="table table-hover table-sm" id="products_table">
            <tr>
                  <th>Nome do Produto</th>
                  <th>Quantidade</th>
                  <th>Preço Unit.</th>
                  <th>Sub-Total</th>
                  <th>Excluir</th>
            </tr>
      </table>

      <hr>

      <input type="submit" value="Adicionar Venda" class="btn btn-secondary btnform"><br><br><br>
</form>

<script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/script_sales_add.js"></script>