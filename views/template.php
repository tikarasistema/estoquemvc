<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no">
		<title>Sistema de Estoque</title>
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/style.css">
		<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript">var BASE_URL = '<?php echo BASE_URL; ?>';</script>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">

			<a class="navbar-brand" href="<?php echo BASE_URL; ?>">Sistema de Estoque</a>

			<!-- <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarMenu">
				<span class="navbar-toggler-icon"></span>
			</button>-->

			<div class="navbar-collapse collapse" id="navbarMenu">

				<div class="navbar-nav">
						
				</div>

			</div>
			<img src="<?php echo BASE_URL; ?>assets/images/avatar.png" width="20"><span class="userName"><?php echo $viewData['name']; ?> | </span>
			<a href="<?php echo BASE_URL; ?>login/sair"><img src="<?php echo BASE_URL; ?>assets/images/sair.png" width="20"></a>
			
		</nav>
	<br/>
	<div class="container">
		<?php
		$this->loadViewInTemplate($viewName, $viewData);
		?>
	</div>

	<!--<footer class="footer">Todos os direitos reservados</footer>-->
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.bundle.min.js"></script>
	</body>

</html>
