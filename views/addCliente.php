<h1>Adicionar Cliente</h1><br>

<?php if(!empty($msg)) {echo $msg;} ?>

<form method="POST">
  <div class="form-row">
  	<div class="form-group col-md-8">
      Nome:
      <input type="text" class="form-control" name="name" id="name" autocomplete="off" required>
    </div>
    <div class="form-group col-md-4">
      CPF:
      <input type="number" class="form-control" name="cpf" id="cpf" autocomplete="off" required>
    </div>
    <div class="form-group col-md-8">
      E-mail:
      <input type="email" class="form-control" name="email" autocomplete="off" id="email">
    </div>
    <div class="form-group col-md-4">
      Telefone:
      <input type="text" class="form-control" name="phone" id="phone" autocomplete="off" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-2">
      CEP:
      <input type="text" class="form-control" name="address_zipcode" id="address_zipcode" autocomplete="off" required>
    </div>
  	<div class="form-group col-md-5">
      Nome da Rua:
      <input type="text" class="form-control" name="address_street" id="address_street" autocomplete="off" required>
    </div>
    <div class="form-group col-md-1">
      Nº
      <input type="text" class="form-control" name="address_number" id="address_number" required>
    </div>
    <div class="form-group col-md-4">
      Bairro:
      <input type="text" class="form-control" name="address_neighb" id="address_neighb" required>
    </div>
    <div class="form-group col-md-3">
      Complemento:
      <input type="text" class="form-control" name="address_complement" id="address_complement" autocomplete="off" required>
    </div>
  </div>
  <div class="form-row">
  	<div class="form-group col-md-4">
      Cidade:
      <input type="text" class="form-control" name="address_city" id="address_city" autocomplete="off" required>
    </div>
    <div class="form-group col-md-4">
      Estado:
      <input type="text" class="form-control" name="address_state" id="address_state" autocomplete="off" required>
    </div>
    <div class="form-group col-md-4">
      País:
      <input type="text" class="form-control" name="address_country" id="address_country" autocomplete="off" required>
    </div>
  </div>
  
	<input type="submit" value="Gravar Cliente" class="btn btn-secondary btnform" /><a class="btn btn-secondary add btnform" href="<?php echo BASE_URL; ?>cliente" role="button" >Voltar</a><br /><br /><br /><br />

</form>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script_clientes_add.js"></script>