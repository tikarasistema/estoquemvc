<form method="GET">
	<input type="text" id="busca" value="<?php echo (!empty($_GET['busca']))?$_GET['busca']:''; ?>" name="busca" placeholder="Digite o nome ou CPF do cliente" style="font-size: 18px;"/>
</form>
<br/>
<a class="btn btn-secondary add" href="<?php echo BASE_URL; ?>cliente/add" role="button">Adicionar Cliente</a><br><br>
<div class="table-responsive">
	<table class="table table-hover table-sm">
		<thead>
		<tr>
			<th>Nome</th>
			<th>CPF</th>
			<th>Email</th>
			<th>Telefone</th>
			<th>Ações</th>
		</tr>
		</thead>
		<?php foreach($list as $item): ?>
		<tbody>
		<tr>
			<td><?php echo $item['name']; ?></td>
			<td><?php echo $item['cpf']; ?></td>
			<td><?php echo $item['email']; ?></td>
			<td><?php echo $item['phone']; ?></td>
			<td>
				<a href="<?php echo BASE_URL; ?>cliente/edit/<?php echo $item['id']; ?>" role="button"><img src="<?php echo BASE_URL; ?>assets/images/edit.png" width="30" title="Editar"></a> | <a href="<?php echo BASE_URL; ?>cliente/delete/<?php echo $item['id']; ?>" data-confirm="Tem certeza que deseja excluir este cliente?" role="button"><img src="<?php echo BASE_URL; ?>assets/images/delete.png" width="30" title="Excluir"></a>
			</td>
		</tr>
		</tbody>
		<?php endforeach; ?>
	</table>
</div>

<hr>
<div class="pag">
	<?php for($q=1;$q<=$paginas;$q++): ?>
		<?php if($paginaAtual == $q): ?>
			<a style="background-color: #333;" href="<?php echo BASE_URL; ?>cliente?<?php
					$w = $_GET;
					$w['p'] = $q;
					echo http_build_query($w);
					?>"><?php echo ($q); ?></a>
		<?php else: ?>
			<a href="<?php echo BASE_URL; ?>cliente?<?php
					$w = $_GET;
					$w['p'] = $q;
					echo http_build_query($w);
					?>"><?php echo ($q); ?></a>
		<?php endif; ?>
	<?php endfor; ?>
</div>
<hr>
<script type="text/javascript">
	document.getElementById("busca").focus();
</script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script_clients_del.js"></script>