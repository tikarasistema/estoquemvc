<h1>Adicionar Produto</h1><br>

<?php if(!empty($msg)) {echo $msg;} ?>

<form method="POST">
	Código de Barras:
	<input type="text" name="cod" id="cod" required class="form-control" autocomplete="off"/><br />
	Nome do Produto:
	<input type="text" name="name" id="name" required class="form-control" autocomplete="off"/><br />
	Categoria:
	<select name="category" id="category" class="form-control">
		<option>Selecione a Categoria</option>
		<?php foreach ($cats as $cat): ?>
			<option value="<?php echo $cat['id']; ?>"><?php echo utf8_encode($cat['name']); ?></option>
		<?php endforeach; ?>
	</select><br />
	Preço:
	<input type="text" name="price" id="price" required class="form-control" autocomplete="off"/><br />
	Quantidade:
	<input type="number" name="quantity" id="quantity" required class="form-control" autocomplete="off"/><br />
	Quantidade Mínima:
	<input type="number" name="min_quantity" id="min_quantity" required class="form-control" autocomplete="off"/><br />

	<input type="submit" value="Gravar Produto" class="btn btn-secondary btnform" /><a class="btn btn-secondary add btnform" href="<?php echo BASE_URL; ?>produto" role="button" >Voltar</a><br /><br /><br />

</form>